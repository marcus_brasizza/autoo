<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrateGuruRespostas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guru_respostas', function (Blueprint $table) {
            $table->increments('id');
            $table->text('resposta');
            $table->integer('id_usuario');
            $table->text('imagem_resposta');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guru_respostas', function (Blueprint $table) {
            //
        });
    }
}
