<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuruResposta extends Model
{

	protected $fillable = [
        'resposta', 'id_usuario','titulo', 'id_guru','link_resposta'
    ];


       public function usuario(){
    	 return $this->belongsTo('App\User','id_usuario');
    }

    public function guru(){
    	return $this->belongsTo('App\Guru','id_guru');
    }


}
