@extends('adminlte::page')
@section('content_header')
<h1>Pergunte para o Guru dos Carros!</h1>
@stop

@section('content')
<style type="text/css">


</style>
<div class="container-fluid">

	As respostas nesta seção são realizadas por César Tizo, um fanático por automóveis e tudo que cerca o tema. Profissional com mais de 10 anos experiência na área, sabe que escolher um carro novo não é uma tarefa fácil e vai colocar toda sua experiência para ajudá-lo(a). Se você está em dúvida sobre algum modelo ou quer alguma indicação por faixa de preço, mande uma mensagem para o Guru dos Carros!<br><br>

	<strong>
		Vale destacar que enviando sua pergunta pelo formulário ao lado ou em nossa página no Facebook você estará autorizando a publicação da mesma de forma integral, incluindo seu nome. As opiniões sobre a escolha do carro são apenas sugestões, isentando o autor de qualquer responsabilidade sobre problemas ou outras pendências envolvendo o veículo indicado.
	</strong>
	<br><br>
	<form method="POST"  name='responder' action="{{route('cadastrar_resposta')}}" enctype="multipart/form-data"> 


		{{ csrf_field() }}
		<input type="hidden" name="id_guru" value="{{$pergunta->id}}">
		<input type="hidden" name="id_usuario_pergunta" value="{{$pergunta->id_usuario}}">
		<input type="hidden" name="id_usuario" value="{{Auth::user()->id}}">
		<div class="col-md-12">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
               <img src="{{($pergunta->usuario->avatar == '') ? asset('default-avatar.png'): $pergunta->usuario->avatar}}" class="img-circle" alt="User Image">
                <span class="username"><a href="#">{{$pergunta->usuario->name}}</a></span>
                <span class="description">{{$pergunta->created_at->format('d/m/Y H:i:s')}}</span>
              </div>
              <!-- /.user-block -->
              <!-- <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Mark as read">
                  <i class="fa fa-circle-o"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div> -->
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <p>{!! nl2br($pergunta->mensagem) !!}</p>
            </div>
            <!-- /.box-body -->
          
            <!-- /.box-footer -->
            <div class="box-footer">
              <form action="#" method="post">
                <img src="{{(Auth::user()->avatar == '') ? asset('default-avatar.png'): 'Auth::user()->avatar'}}" class="img-responsive img-circle img-sm" alt="User Image" width="30" height="30">
                <!-- .img-push is used to add margin to elements next to floating images -->
                <div class="img-push">
                		<div class="form-group">
                  <label>Titulo</label>
                  <input type="text" class="form-control" placeholder="Titulo" name="titulo" required=""> 
                </div>
                 

                  <textarea  name='resposta' id='resposta' placeholder="Press enter to post comment" cols="50" rows="30"> </textarea>
                </div>
              
              </form>
            </div>
            <!-- /.box-footer -->
          <!-- /.box -->
        
        <a class="btn btn-app" onclick="document.responder.submit()">
                <i class="fa fa-save" ></i> Enviar resposta
              </a>
          </div>
        </div>
	</form>
@stop

