@extends('adminlte::page')
@section('content_header')
<h1>Pergunte para o Guru dos Carros!</h1>
@stop

@section('content')
<style type="text/css">


</style>
<div class="container-fluid">

	As respostas nesta seção são realizadas por César Tizo, um fanático por automóveis e tudo que cerca o tema. Profissional com mais de 10 anos experiência na área, sabe que escolher um carro novo não é uma tarefa fácil e vai colocar toda sua experiência para ajudá-lo(a). Se você está em dúvida sobre algum modelo ou quer alguma indicação por faixa de preço, mande uma mensagem para o Guru dos Carros!<br><br>

	<strong>
		Vale destacar que enviando sua pergunta pelo formulário ao lado ou em nossa página no Facebook você estará autorizando a publicação da mesma de forma integral, incluindo seu nome. As opiniões sobre a escolha do carro são apenas sugestões, isentando o autor de qualquer responsabilidade sobre problemas ou outras pendências envolvendo o veículo indicado.
	</strong>
	<br><br>
	<form method="POST" action="{{route('guru_cadastrar')}}" enctype="multipart/form-data"> 
		{{ csrf_field() }}
		<input type="hidden" name="id_usuario" value="{{Auth::user()->id}}">
		<div class="form-group col-lg-6 col-sm-12">
			<label for="mensagem">Faça sua pergunta</label>
			<textarea class="form-control" id="mensagem"  name="mensagem" rows=10 required></textarea>
		</div>

		<span class="clearfix"></span>
		<div class="panel panel-default col-lg-6 col-sm-12">
			<div class="panel-heading ">Dados complementares</div>
			<div class="panel-body">
				<div class="form-group col-lg-6 col-sm-12">
					<label for="exampleFormControlSelect1">Marca</label>
					<select class="form-control" id="marca" name='marca'>
						<option value="-1">--Selecione</option>
						@foreach($marcas as $marca)
						<option value={{$marca->id}}> {{$marca->name}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-lg-6 col-sm-12">
					<div>
						<label class="demo-label">Modelo</label><br/> <input type="search" autocomplete="off" name="modelo" id="modelo" class="typeahead" autocomplete="false" />
					</div>

					<hr>

				</div>
			</div>
		</div>
		<span class="clearfix"></span>
		<div class="form-group col-lg-6 col-sm-12">
			<button class="btn btn-primary btn-lg type="submit">Enviar</button>
		</div>
	</form>

	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="text-center">Perguntas enviadas</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tbody><tr>
						<th>#</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Status</th>
					</tr>
					@foreach($perguntas as $pergunta)
					<tr>
						<td>{{$pergunta->id}}</td>
						<td>{{$pergunta->marca}}</td>
						<td>{{$pergunta->modelo}}</td>


						<td>



								@if($pergunta->respondido == 0)
								
								<span class="label label-info">Aguardando</span>
								@else

								@if(isset($pergunta->resposta->link_resposta))

									<span class="label label-success">
									<a  target="_blank" href="{{route('mostrar-respondido',['id' => $pergunta->resposta->link_resposta])}}" style="color:white">Respondido</a>
								</span>
								@endif


								
								@endif
							
						</td>
					</tr>
					@endforeach
				</tbody></table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
@stop

