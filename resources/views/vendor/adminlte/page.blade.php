@extends('adminlte::master')

@section('adminlte_css')
<link rel="stylesheet"
href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
@stack('css')
@yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
'boxed' => 'layout-boxed',
'fixed' => 'fixed',
'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    @if(config('adminlte.layout') == 'top-nav')
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="{{ url(config('adminlte.dashboard_url', 'administrativo.home')) }}" class="navbar-brand">
            {!! config('adminlte.logo', '<b>Admin</b>LTE') !!}
          </a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">

            @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        @else
        <!-- Logo -->
        <a href="{{ url(config('adminlte.dashboard_url', 'administrativo.home')) }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>A</b>LT') !!}</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>



        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
          </a>

          <div class="navbar-custom-menu hidden-sm-up">
            <ul class="nav navbar-nav">
            @if(Auth::user()->nivel == 0)
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>

                  <span class="label label-success">{{count($notificacoes['NovaPergunta'])}}</span>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      @foreach($notificacoes['NovaPergunta'] as $notificacao)
                      <li><!-- start message -->
                        <a href="{{route('responder_guru', ['id'=>$notificacao->id , 'notificacao' => $notificacao->id_notification])}}">
                         
                          <h4>
                            Pergunta #{{$notificacao->id}}
                            <small><i class="fa fa-clock-o"></i>{{$notificacao->created_at->format('d/m/Y H:i:s')}}</small>
                          </h4>
                          <p>Clique para saber a pergunta</p>

                          @if($notificacao->turbinado == 1)
                          <span class="pull-right-container">
                            <small class="label bg-red">Turbinado!</small>
                          </span>
                          @endif
                        </a>
                      </li>
                      @endforeach
                      <!-- end message -->
                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
              @endif
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">{{count($notificacoes['NovaResposta'])}}</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Voce tem {{count($notificacoes['NovaResposta'])}} resposta(s) respondida(s)</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                       @foreach($notificacoes['NovaResposta'] as $notificacao)

                      <li>
                        <a  target="_blank" href="{{route('mostrar-respondido',['id' => $notificacao->link_resposta , 'notification' => $notificacao->id_notification])}}">
                          <i class="fa fa-user text-red"></i>{{$notificacao->usuario->name}} respondeu
                         <small class="text-sm">
                          <div>
                            {{$notificacao->titulo}}
                          </div>
                          </small>
                        </a>

                      </li>
                      @endforeach
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li>
              <!-- Tasks: style can be found in dropdown.less -->
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <span class="hidden-xs">{{Auth::user()->name}}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{(Auth::user()->avatar == '') ? asset('default-avatar.png'): 'Auth::user()->avatar'}}" class="img-circle" alt="User Image">
                    <p>

                     {{Auth::user()->name}}
                   </p>
                 </li>
                 <!-- Menu Body -->
                 <li class="user-body">
                  <div class="row">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="#" class="btn btn-default btn-flat"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                    >
                    {{ trans('adminlte::adminlte.log_out') }}
                  </a>
                  <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                    @if(config('adminlte.logout_method'))
                    {{ method_field(config('adminlte.logout_method')) }}
                    @endif
                    {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
      @endif

      @if(config('adminlte.layout') == 'top-nav')
    </div>
    @endif
  </nav>
</header>

@if(config('adminlte.layout') != 'top-nav')
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{(Auth::user()->avatar == '') ? asset('default-avatar.png'): 'Auth::user()->avatar'}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{Auth::user()->name}}</p>
      </div>
    </div>
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>
@endif

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  @if(config('adminlte.layout') == 'top-nav')
  <div class="container">
    @endif

    <!-- Content Header (Page header) -->
    <section class="content-header">
      @yield('content_header')
    </section>

    <!-- Main content -->
    <section class="content">

      @yield('content')

    </section>
    <!-- /.content -->
    @if(config('adminlte.layout') == 'top-nav')
  </div>
  <!-- /.container -->
  @endif
</div>
<!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->
@stop

@section('adminlte_js')
<script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
@stack('js')
@yield('js')
@stop
