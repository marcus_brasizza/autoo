<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Autoo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">


    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.min.css') }}">

    

    @yield('adminlte_css')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
    <body>
       <div class="container" style="min-height: 1080px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$resposta->titulo}}
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <!-- The time line -->
          <ul class="timeline">
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-red">
                    {{$resposta->guru->created_at->format('d/m/Y')}}
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i>{{$resposta->guru->created_at->format('H:i:s')}}</span>

                <h3 class="timeline-header"><a href="#">{{$resposta->guru->usuario->name}}</a> perguntou</h3>

                <div class="timeline-body">
                 {!! $resposta->guru->mensagem !!}
                </div>
             
              </div>
            </li>
            <li class="time-label">
                  <span class="bg-yellow">
                    {{$resposta->created_at->format('d/m/Y')}}
                  </span>
            </li>
            <li>
              <i class="fa fa-comments bg-yellow"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i>{{$resposta->created_at->format('H:i:s')}}</span>

                <h3 class="timeline-header"><a href="#">{{$resposta->usuario->name}}</a></h3>

                <div class="timeline-body">

                    <div class="box-body">

<div class="col-md-10 blogShort">
                     <h1>{{$resposta->titulo}}</h1>
                    
                     <article>{!! $resposta->resposta !!}
                     
                     </article>
                
                 </div>     
            </div>
              
                </div>
               
              </div>
            </li>
           
        
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</section>
    <!-- /.content -->
  </div>
    </body>
</html>
