@extends('adminlte::page')
@section('content_header')
<h1>Pergunte para o Guru dos Carros!</h1>
@stop

@section('content')
<style type="text/css">


</style>
<div class="container-fluid">

	As respostas nesta seção são realizadas por César Tizo, um fanático por automóveis e tudo que cerca o tema. Profissional com mais de 10 anos experiência na área, sabe que escolher um carro novo não é uma tarefa fácil e vai colocar toda sua experiência para ajudá-lo(a). Se você está em dúvida sobre algum modelo ou quer alguma indicação por faixa de preço, mande uma mensagem para o Guru dos Carros!<br><br>

	<strong>
		Vale destacar que enviando sua pergunta pelo formulário ao lado ou em nossa página no Facebook você estará autorizando a publicação da mesma de forma integral, incluindo seu nome. As opiniões sobre a escolha do carro são apenas sugestões, isentando o autor de qualquer responsabilidade sobre problemas ou outras pendências envolvendo o veículo indicado.
	</strong>
	<br><br>
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<h3 class="text-center">Perguntas aguardando resposta</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table class="table table-hover">
					<tbody><tr>
						<th>#</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Usuario</th>
						<th>Status</th>
						<th></th>
					</tr>
					@foreach($perguntas as $pergunta)
					<tr>
						<td><a href="{{route('responder_guru', ['id'=>$pergunta->id])}}">{{$pergunta->id}}</a></td>
						<td>{{$pergunta->marca}}</td>
						<td>{{$pergunta->modelo}}</td>
						<td>{{$pergunta->usuario->name}}</td>
						<td><span class="label label-{{$pergunta->respondido == 0 ? 'info' : 'success'}}">{{$pergunta->respondido == 0 ? 'Aguardando' : 'Respondido' }}</span></td>
						<td>
								@if($pergunta->turbinado == 1)
							<span class="pull-right-container">
              <small class="label bg-red">Turbinado!</small>
            </span>
            @endif
        </td>
					</tr>
					@endforeach
				</tbody></table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
@stop

