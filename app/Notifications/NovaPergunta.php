<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\DatabaseMessage;

use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;


class NovaPergunta extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($guru)
    {
        $this->guru = $guru;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
            if($this->guru->usuario->telegram_id != ''){
                return ['mail', 'database', TelegramChannel::class];
            }else{
            return ['mail', 'database'];
                
            }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
   public function toArray($notifiable)
    {
        return [
            'message' => 'A new post was published on Laravel News.',
            'action' => url($this->post->slug)
        ];
    }

    public function toDatabase($notifiable)
{
    return new DatabaseMessage($this->guru->toArray());
}

public function toTelegram($notifiable)
    {   



       

        return TelegramMessage::create()
            ->to($this->guru->usuario->telegram_id) // Optional.
            ->content("*HELLO!* \n One of your invoices has been paid!") ;// Markdown supported.
        
        }


}
