<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


   	Route::get('/admin', 'AdminController@index')->name('admin')->middleware('auth');
   	Route::get('/guru/pergunte', 'GuruAutomovelController@index')->name('perguntas_guru')->middleware('auth');
   	Route::get('/guru/responde', 'GuruAutomovelController@responder')->name('respostas_guru')->middleware('auth');
   	
   	Route::get('/guru/responder/{id}/{notification?}', 'GuruAutomovelController@resposta')->name('responder_guru')->middleware('auth');
   	Route::get('/guru/respondido/{id}/{notification?}', 'GuruAutomovelController@mostrarResposta')->name('mostrar-respondido');
   

   	Route::post('guru/cadastrar', 'GuruAutomovelController@Cadastrar')->name('guru_cadastrar')->middleware('auth');
   	Route::post('guru/modelo', 'GuruAutomovelController@ProcurarModelo')->middleware('auth');
   	Route::post('guru/resposta', 'GuruAutomovelController@salvarResposta')->name('cadastrar_resposta')->middleware('auth');
  

Route::get('facebook', function () {
    return view('facebook');
});


Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');
Auth::routes();