<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	  DB::table('users')->insert([
            'name' => 'Administrador 1',
            'email' => 'admin@admin.com.br',
            'password' => bcrypt('secret'),
            'nivel'=>'0',
            'status'=>'1'
        ]);
        
    }
}
