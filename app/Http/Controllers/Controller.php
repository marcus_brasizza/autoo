<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;
use App\Guru;
use App\GuruResposta;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function notificacoes(){
    	   $notificacoes['NovaPergunta'] = array();
    	   $notificacoes['NovaResposta'] = array();
        foreach (Auth::user()->unreadNotifications as $idx=>$notification) {
                $dados = explode('\\',$notification->type);
               
                if($dados[2] == 'NovaPergunta'){
                $notificacoes["{$dados[2]}"][$idx] = Guru::find(($notification->data['id']));
                $notificacoes["{$dados[2]}"][$idx]['id_notification'] = $notification->id;
                    
                }elseif($dados[2] == 'NovaResposta'){
                	$notificacoes["{$dados[2]}"][$idx] = GuruResposta::find(($notification->data['id']));
                	if(isset($notificacoes["{$dados[2]}"][$idx])){
               		$notificacoes["{$dados[2]}"][$idx]['id_notification'] = $notification->id;
               	}else{
               	}
                }
                
            }
            return $notificacoes;
    }
}
