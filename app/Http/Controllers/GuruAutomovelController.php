<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guru;
use App\GuruResposta;
use App\User;
use Auth;
use Storage;

use Illuminate\Support\Facades\Notification;
use App\Notifications\NovaPergunta;
use App\Notifications\NovaResposta;

class GuruAutomovelController extends Controller
{
    public function index(Request $request){
    	if($request->session()->has('marcas')){
    		$marcas = $request->session()->get('marcas');
    	}else{

    	$marcas = json_decode(file_get_contents('http://fipeapi.appspot.com/api/1/carros/marcas.json'));
    	}
    	
    	$request->session()->put('marcas',$marcas);

    	$listagemGuru = Guru::where('id_usuario','=', Auth::user()->id)->get();
    	

    	$dados = array(
    			'marcas' => $marcas,
    			'perguntas'=>$listagemGuru,
    			'notificacoes' => $this->notificacoes()

    	);
    	return view('administrativo.guru.index')->with($dados);


    }
    public function ProcurarModelo(Request $request){

    	$marca = $request->input('marca');
    	
    	return (file_get_contents("http://fipeapi.appspot.com/api/1/carros/veiculos/{$marca}.json"));
    }


    public function responder(Request $request){
    		$listagemGuru = Guru::where('respondido','=',0)->orderBy('turbinado','DESC')->get();
    	$dados = array(
    			'perguntas'=>$listagemGuru,
    			'notificacoes'=>$this->notificacoes()

    	);
    	return view('administrativo.guru.responder')->with($dados);
    	
    }

    public function resposta(Request $request,$id , $notificacao =''){
    	$guru = Guru::find($id);
    		 $notification = Auth::user()->notifications()->where('id',$notificacao)->first();
    			if($notification){
    				$notification->markAsRead();
    			}
    		$dados = array(
    			'pergunta'=>$guru,
    			'notificacoes'=>$this->notificacoes()

    	);


    	return view('administrativo.guru.resposta')->with($dados);
    }



      public function Cadastrar(Request $request){
      	$marcas = $request->session()->get('marcas');
        $marca = $request->get('marca');
    	$guru_fields = ($request->except('_token'));
    		$guru_fields['marca'] = '';
        foreach($marcas as $marcax){
        	if($marcax->id == $marca){

        		$guru_fields['marca'] = $marcax->name;
        	}
        }
        $user = User::where('nivel' , '=','0')->get();
   		$id = Guru::create($guru_fields)->id;
   		$guruGet = Guru::find($id);
        Notification::send($user, new NovaPergunta($guruGet));
   		return redirect()->action('GuruAutomovelController@index');

    }

    public function store(Request $request)
{
    // Define o valor default para a variável que contém o nome da imagem 
    $nameFile = null;
 
    // Verifica se informou o arquivo e se é válido
    if ($request->hasFile('imagem_resposta') && $request->file('imagem_resposta')->isValid()) {
         
        // Define um aleatório para o arquivo baseado no timestamps atual
        $name = uniqid(date('HisYmd'));
 
        // Recupera a extensão do arquivo
        $extension = $request->imagem_resposta->extension();
 
        // Define finalmente o nome
        $nameFile = "{$name}.{$extension}";
 
        // Faz o upload:
        $upload = $request->imagem_resposta->storeAs('posts', $nameFile,'gcs');
        // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao
 		return $upload;
        // Verifica se NÃO deu certo o upload (Redireciona de volta)
        if ( !$upload )
            return redirect()
                        ->back()
                        ->with('error', 'Falha ao fazer upload')
                        ->withInput();
 
    }
}


    public function salvarResposta(Request $request){
    	$arquivo_imagem_post = $this->store($request);
    	$resposta = $request->except('_token');
    	$resposta['imagem_resposta'] = $arquivo_imagem_post;
    	$resposta['link_resposta'] = (str_replace(' ','+',$resposta['titulo']).'-'.uniqid('AUTOO').'-'.date('YmdHis'));

    	$id = GuruResposta::create($resposta)->id;
    	$resposta = GuruResposta::find($id);

    	$user = User::find($request->get('id_usuario_pergunta'));
    	Notification::send($user, new NovaResposta($resposta));

    	$guru = Guru::find($request->id_guru);
    	$guru->respondido = 1 ;
    	$guru->save();
    	return redirect()->action('AdminController@index');

    	
    }

    public function mostrarResposta(Request $request , $id, $notificacao = ''){
			$resposta = GuruResposta::where('link_resposta','=', $id)->first();

			$usuario = User::find($resposta->guru->usuario->id);
				$disk = Storage::disk('gcs');
				$url = $disk->url($resposta['imagem_resposta']);
				if($url){
					$resposta['imagem_resposta'] = $url;
				}else{
					$resposta['imagem_resposta'] ='';
				}

				 $notification = $usuario->notifications()->where('id',$notificacao)->first();
    			if($notification){
    				$notification->markAsRead();
    			}
			return view('resposta_guru')->with('resposta', $resposta);

    }
}
