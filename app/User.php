<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','facebook_id','avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function addNew($input)
    {   

        $check = static::where('facebook_id',$input['facebook_id'])->first();

        if(is_null($check)){
         
            return static::create($input);
           
        }else{
         

            $check['avatar'] = $input['avatar'];
            $check->save();

        }
        return $check;
    }

    public function guru(){
        return $this->hasMany("App\Guru", 'id_usuario');
    }

}
