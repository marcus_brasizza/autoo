<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
     protected $fillable = [
        'mensagem', 'marca','modelo','turbinado','id_usuario'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    public function usuario(){
    	 return $this->belongsTo('App\User','id_usuario');
    }


    public function resposta(){
    	return ($this->belongsTo('App\GuruResposta','id' , 'id_guru'));
    }
  
}
