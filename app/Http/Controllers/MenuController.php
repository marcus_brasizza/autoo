<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    //

	public function __construct($event){

		$this->event = $event;
		return $this;
	}

	public function makeMenu(){
		$this->event->menu->add('MAIN NAVIGATION');
		$this->event->menu->add([
			'text' => 'Guru do Automovel',
			'url' => route('perguntas_guru'),
			'can'=>'menu-geral',
			'icon' => 'car',
		]);

$this->event->menu->add([
			'text' => 'Responder Guru',
			'url' => route('respostas_guru'),
			'can'=>'menu-administrativo',
			'icon' => 'car',
		]);




	}
}
